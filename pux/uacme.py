# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
Functions to integrate with uacme
"""

import asyncio
import logging

from pux.tools import config
from pux.tools.constants import PUX_EXE
from pux.tools.exceptions import UacmeError

LOGGER = logging.getLogger(__name__)


async def execute(arguments, raise_on_error=True):
    """
    Execute uacme on the current machine

    It passes arguments to uacme
    If raise_on_error is True nad uacme returns any values except 0 or 1, it will raise a UacmeError with the value on stderr
    """
    if not isinstance(arguments, list) or any(not isinstance(x, str) for x in arguments):
        raise TypeError("Arguments must be a list of strings")

    args = ["--confdir", config["uacme"]["path"], "--hook", PUX_EXE]
    if config["uacme"]["url"]:
        args.extend(["--acme-url", config["uacme"]["url"]])
    args.extend(arguments)
    process = await asyncio.create_subprocess_exec("uacme", *args, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await process.communicate()
    if process.returncode not in (0, 1):
        LOGGER.error("uacme error: %s", stderr)
        if raise_on_error:
            raise UacmeError(stderr.decode("ascii"))

    return process.returncode, stdout.decode("ascii"), stderr.decode("ascii")


async def issue(names, raise_on_error=True):
    """
    Issue a certificate for names
    """
    if isinstance(names, str):
        names = names.split(" ")
    return await execute(["--bits", str(config["uacme"]["bits"]), "--type", config["uacme"]["type"], "issue"] + names, raise_on_error)
