# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
Functions to integrate with pux servers
"""

import asyncio
import base64
import collections
import contextlib
import io
import logging

from cryptography import fernet

from pux.tools import config, constants
from pux.tools.exceptions import InvalidToken

LOGGER = logging.getLogger(__name__)

PuxResult = collections.namedtuple("PuxResult", ["returncode", "message"])
PuxConnection = collections.namedtuple("PuxConnection", ["reader", "writer"])

__all__ = ["decrypt", "encrypt", "send", "sendfile"]


def decrypt(message, decode=True):
    """
    Decrypt a pux message

    If decode is True, it will return the decoded message as a string
    """
    enc = fernet.Fernet(config["pux"]["key"].encode())
    try:
        response = enc.decrypt(message)
    except fernet.InvalidToken as err:
        # We use a custom exception in case we change the encryption method
        raise InvalidToken("Invalid token") from err
    if decode:
        return response.decode("ascii")
    return response


def encrypt(message):
    """
    Encrypts a plain-text message (either a string or bytes) to send to pux
    """
    if isinstance(message, str):
        message = message.encode("ascii")
    enc = fernet.Fernet(config["pux"]["key"].encode())
    return enc.encrypt(message)


@contextlib.asynccontextmanager
async def _open_pux(server):
    """
    Context manager to open a connection to a pux server
    """
    host = server.split(":")[0]
    try:
        port = int(server.split(":")[1])
    except IndexError:
        port = constants.PUX_TCP_PORT

    reader, writer = await asyncio.open_connection(host, port)
    con = PuxConnection(reader, writer)
    try:
        yield con
    finally:
        writer.close()


async def send(*command):
    """
    Sends a command to all configured pux server

    All positional arguments of this functions must be strings and they are sent
    to pux in order
    """
    if not command:
        return {}, False
    if any(not isinstance(x, str) for x in command):
        raise TypeError("Commands must be a list of strings")
    cmd = " ".join(command)

    payload = io.BytesIO(encrypt(cmd))

    result = {}
    failed = False

    for server in config["pux"]["servers"]:
        LOGGER.debug("Sending %s to %s", command[0], server)
        payload.seek(0)

        try:
            async with _open_pux(server) as con:
                package = payload.read(constants.SERVER_CHUNK_SIZE)
                while package:
                    con.writer.write(package)
                    LOGGER.debug("Sending chunk to %s...", server)
                    await con.writer.drain()
                    package = payload.read(constants.SERVER_CHUNK_SIZE)

                con.writer.write_eof()
                LOGGER.debug("Command sent to %s. Waiting for response", server)
                response = io.BytesIO()
                while not con.reader.at_eof():
                    package = await con.reader.read(constants.SERVER_CHUNK_SIZE)
                    response.write(package)
                    LOGGER.debug("Received from %s", server)

                response.seek(0)
                plain = decrypt(response.read()).split(":")
                response.close()
        except (ConnectionRefusedError, OSError):
            plain = ["1", "connection refused"]
        except InvalidToken:
            plain = ["1", "invalid token"]
        except Exception:
            LOGGER.exception("Unable to send %s to %s", command[0], server)
            plain = ["1", "unknown error"]
        status = int(plain[0])
        message = ":".join(plain[1:])
        if status > constants.SERVER_RCODE_OK:
            LOGGER.warning("pux server %s returned a error: %s", server, message)
            failed = True
        result[server] = PuxResult(status, message)
    return result, failed


async def sendfile(remotepath, localpath):
    """
    Reads a local file (localpath), encode his contents in base64 and sends it to all pux servers
    """
    LOGGER.debug("Sending %s to servers as %s", localpath, remotepath)
    with open(localpath, "rb") as local:
        content = base64.b64encode(local.read())
    return await send("install", remotepath, content.decode("ascii"))
