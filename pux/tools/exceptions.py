# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
Custom exceptions used by pux
"""


class InvalidToken(Exception):
    """Used when a fernet token can't be decrypted"""


class UacmeError(Exception):
    """When uacme fails"""


class UalpnError(Exception):
    """When ualpn fails"""


class InvalidConfiguration(Exception):
    """Invalid configuration"""


class InvalidCommand(Exception):
    """Used when a invalid command is specified"""
