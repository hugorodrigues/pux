# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
Application level constants
"""

import os
import sys
import warnings

PUX_EXE = os.path.abspath(sys.argv[0])

if PUX_EXE.split(os.path.sep)[-1] == "__main__.py":
    warnings.warn("When pux is executed as a python module, uacme will not work", RuntimeWarning)


PUX_TCP_HOST = "0.0.0.0"
PUX_TCP_PORT = 9003
PUX_CONFIG = os.environ.get("PUX_CONFIG", "/etc/pux.ini")

UALPN_SOCKET = "/var/run/ualpn.sock"

UACME_PATH = "/etc/uacme.d"
UACME_BITS = 384
UACME_TYPE = "EC"
UACME_CHALLENGE = "http-01"

NGINX_CONFIG = "/etc/nginx/nginx.conf"
NGINX_WELLKNOWN = "/var/www/.well-known/acme-challenge"

# Exit codes
ECODE_OK = 0
ECODE_INVALID_COMMAND = 1
ECODE_CROSSPLANE_NOT_INSTALLED = 2
ECODE_UACME_DENY = 3
ECODE_FORCED_SHUTDOWN = 4
ECODE_CERT_INSTALL_FAILED = 5

# Server returncode
SERVER_RCODE_OK = 0
SERVER_RCODE_INVALID_TOKEN = 1
SERVER_RCODE_UNKNOWN_ERROR = 2
SERVER_RCODE_INVALID_COMMAND = 3
SERVER_RCODE_UALPN_ERROR = 4
SERVER_RCODE_INVALID_CHALLENGE = 5
SERVER_RCODE_SYS_ERROR = 6

SERVER_CHUNK_SIZE = 1024
SERVER_REQUEST_MAX_BYTES = 5120  # 5 kB - Each certificate is around 3kB
