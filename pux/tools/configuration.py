# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
Configuration parser
"""

from collections.abc import Mapping
import configparser
import logging
import os
import types

from pux.tools import constants
from pux.tools.exceptions import InvalidConfiguration


class ConfigurationMapping(Mapping):
    """
    Base class for configuration elements.

    It ensures that internal values are readonly
    """

    def __init__(self, values):
        self._values = types.MappingProxyType(values)

    def __getitem__(self, value):
        return self._values[value]

    def __len__(self):
        return len(self._values)

    def __iter__(self):
        return iter(self._values)


class ConfigurationSection(ConfigurationMapping):
    """
    Represents each section inside the ini file
    """

    def __init__(self, parser, name, elements):
        values = {}
        self._name = name
        for key, default in elements.items():
            try:
                value = parser[name][key]
                if isinstance(default, int):
                    value = int(value)
                elif isinstance(default, bool):
                    value = value in ("true", "True", "1", "y", "Y")
                elif isinstance(default, list):
                    value = [x.strip() for x in value.split(",")]
                values[key] = value
            except (KeyError, ValueError):
                values[key] = default
        super().__init__(values)

    def __str__(self):
        return self._name


class Configuration(ConfigurationMapping):
    """
    pux configuration
    """
    _defaults = types.MappingProxyType({
        "pux": types.MappingProxyType({
            "host": constants.PUX_TCP_HOST,
            "port": constants.PUX_TCP_PORT,
            "maxrequest": constants.SERVER_REQUEST_MAX_BYTES,
            "servers": [],
            "key": None,
            "loglevel": logging.INFO,
            "syslog": False,
            "echo": False
        }),
        "ualpn": types.MappingProxyType({
            "socket": constants.UALPN_SOCKET
        }),
        "uacme": types.MappingProxyType({
            "path": constants.UACME_PATH,
            "bits": constants.UACME_BITS,
            "type": constants.UACME_TYPE,
            "url": None,
            "challenge": constants.UACME_CHALLENGE
        }),
        "nginx": types.MappingProxyType({
            "config": constants.NGINX_CONFIG,
            "wellknown": constants.NGINX_WELLKNOWN
        })
    })

    def __init__(self):
        if not os.path.exists(constants.PUX_CONFIG):
            raise FileNotFoundError(f"Configuration file {constants.PUX_CONFIG} not found")
        values = {}
        parser = configparser.ConfigParser()
        parser.read(constants.PUX_CONFIG)
        for section, elements in self._defaults.items():
            values[section] = ConfigurationSection(parser, section, elements)
        super().__init__(values)

        if self["uacme"]["challenge"] not in ("tls-alpn-01", "http-01"):
            raise InvalidConfiguration("Use tls-alpn-01 or http-01 as uacme challenge")
        if not self["pux"]["key"]:
            raise InvalidConfiguration("pux.key must be set")


config = Configuration()
