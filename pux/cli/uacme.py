# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
Commands used as a hook for uacme
"""

import collections
import logging
import sys

from pux import pux, uacme
from pux.tools import config, constants

LOGGER = logging.getLogger(__name__)

__all__ = ["begin", "done", "failed", "new"]

try:
    UACME_ARGS = collections.namedtuple("UACME", "type ident token auth")._make([sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]])
    LOGGER.debug(UACME_ARGS)
except IndexError:
    UACME_ARGS = None
    LOGGER.debug("Unable to parser uacme args: %s", sys.argv)


def _validate_type():
    if not UACME_ARGS or UACME_ARGS.type != config["uacme"]["challenge"]:
        LOGGER.debug("UACME_ARGS not set or type is not valid")
        return False
    return True


async def begin():
    """
    uacme begin hook
    """
    if not _validate_type():
        return constants.ECODE_UACME_DENY

    res, nok = await pux.send("auth", UACME_ARGS.type, UACME_ARGS.ident, UACME_ARGS.token, UACME_ARGS.auth)
    if nok:
        for server, status in dict(filter(lambda x: x[1].returncode > 0, res.items())).items():
            LOGGER.error("Unable to add authentication in %s: %s", server, status.message)
        await pux.send("unauth", UACME_ARGS.type, UACME_ARGS.ident, UACME_ARGS.token, UACME_ARGS.auth)
        return constants.ECODE_UACME_DENY
    return constants.ECODE_OK


async def done():
    """
    uacme done hook
    """
    if not _validate_type():
        return constants.ECODE_UACME_DENY

    res, nok = await pux.send("unauth", UACME_ARGS.type, UACME_ARGS.ident, UACME_ARGS.token, UACME_ARGS.auth)
    if nok:
        for server, status in dict(filter(lambda x: x[1].returncode > 0, res.items())).items():
            LOGGER.error("Unable to remove authentication in %s: %s", server, status.message)
        return constants.ECODE_UACME_DENY
    return constants.ECODE_OK


async def failed():
    """
    uacme failed hook
    """
    return await done()


async def new():
    """
    uacme new command
    """
    rcode, _, stderr = await uacme.execute(["--yes", "new"], raise_on_error=False)
    if rcode == 2 and "Account already exists at" in stderr:
        return constants.ECODE_OK
    return rcode
