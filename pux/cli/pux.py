# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
pux cli commands
"""


import asyncio
import base64
import io
import logging
import os

from pux import pux, ualpn, uacme
from pux.tools import config, constants, exceptions

LOGGER = logging.getLogger(__name__)

try:
    import crossplane
except ImportError:
    crossplane = None
    LOGGER.warning("crossplane not installed. You will not be able to use the generate command")

__all__ = ["server", "generate"]


async def _request_echo(*args):
    """
    Simple echo server for tests
    """
    return constants.SERVER_RCODE_OK, " ".join(args)


async def _request_auth(challenge, ident, token, auth):
    """
    Adds authentication to local machine
    """
    if challenge == "tls-alpn-01":
        try:
            await ualpn.auth(ident, auth)
            return constants.SERVER_RCODE_OK, "authentication added"
        except exceptions.UalpnError as err:
            LOGGER.exception("unable to add tls-alpn-01 authentication")
            return constants.SERVER_RCODE_UALPN_ERROR, str(err)
    elif challenge == "http-01":
        try:
            with open(os.path.join(config["nginx"]["wellknown"], token), mode="w", encoding="ascii") as cfile:
                cfile.write(auth)
            return constants.SERVER_RCODE_OK, "authentication added"
        except Exception as err:
            LOGGER.exception("unable to add http-01 authentication")
            return constants.SERVER_RCODE_SYS_ERROR, str(err)
    return constants.SERVER_RCODE_INVALID_CHALLENGE, "invalid challenge type"


# Although we don't use the auth argument, uacme sends it anyway
async def _request_unauth(challenge, ident, token, auth):  # pylint: disable=unused-argument
    """
    Removes authentication
    """
    if challenge == "tls-alpn-01":
        try:
            await ualpn.unauth(ident)
            return constants.SERVER_RCODE_OK, "authentication removed"
        except exceptions.UalpnError as err:
            LOGGER.exception("unable to remove tls-alpn-01 authentication")
            return constants.SERVER_RCODE_UALPN_ERROR, str(err)
    elif challenge == "http-01":
        try:
            os.remove(os.path.join(config["nginx"]["wellknown"], token))
            return constants.SERVER_RCODE_OK, "authentication removed"
        except FileNotFoundError:
            return constants.SERVER_RCODE_OK, "authentication removed"
        except Exception as err:
            LOGGER.exception("unable to remove http-01 authentication")
            return constants.SERVER_RCODE_SYS_ERROR, str(err)
    return constants.SERVER_RCODE_INVALID_CHALLENGE, "invalid challenge type"


async def _request_install(path, content):
    """
    Install a file on the local server
    """
    try:
        with open(path, "wb") as local:
            local.write(base64.b64decode(content.encode("ascii")))
    except Exception:
        LOGGER.exception("Unable to create file")
        return constants.SERVER_RCODE_SYS_ERROR, "unable to create files"
    return constants.SERVER_RCODE_OK, "files installed"


async def _server_handler(reader, writer):
    """
    Handle incoming connections
    """
    start = asyncio.get_running_loop().time()
    peer = "%s:%s" % writer.get_extra_info("peername")  # pylint: disable=consider-using-f-string
    LOGGER.info("Connection from %s", peer)
    payload = io.BytesIO()
    payload_size = 0
    while not reader.at_eof() and payload_size < config["pux"]["maxrequest"]:
        package = await reader.read(constants.SERVER_CHUNK_SIZE)
        payload.write(package)
        payload_size += constants.SERVER_CHUNK_SIZE
        LOGGER.debug("Received %d from %s", payload_size, peer)

    payload.seek(0)

    try:
        data = pux.decrypt(payload.read()).split(" ")
        command = data[0]
        arguments = data[1:]

        # select the command
        if config["pux"]["echo"] and command == "echo":
            response = await _request_echo(*arguments)
        elif command == "auth":
            response = await _request_auth(*arguments)
        elif command == "unauth":
            response = await _request_unauth(*arguments)
        elif command == "install":
            response = await _request_install(*arguments)
        else:
            raise exceptions.InvalidCommand()

    except exceptions.InvalidToken:
        LOGGER.warning("Invalid encryption token from %s", peer)
        response = (constants.SERVER_RCODE_INVALID_TOKEN, "invalid token")
    except exceptions.InvalidCommand:
        LOGGER.warning("Invaid command from %s", peer)
        response = (constants.SERVER_RCODE_INVALID_COMMAND, "invalid command")
    except Exception as err:
        LOGGER.exception("Unable to process request")
        response = (constants.SERVER_RCODE_UNKNOWN_ERROR, str(err))

    payload.seek(0)
    payload.truncate(0)
    payload.write(pux.encrypt("%d:%s" % response))  # pylint: disable=consider-using-f-string
    payload.seek(0)
    package = payload.read(constants.SERVER_CHUNK_SIZE)
    while package:
        writer.write(package)
        LOGGER.debug("Sending chunk to %s...", peer)
        await writer.drain()
        package = payload.read(constants.SERVER_CHUNK_SIZE)

    writer.write_eof()
    LOGGER.debug("Connection from %s closed. Took %0.03f ms", peer, asyncio.get_running_loop().time() - start)
    writer.close()
    payload.close()


async def server():
    """
    Runs pux server
    """
    if config["pux"]["echo"]:
        LOGGER.warning("echo command is enabled. Use only for testing.")
    aioserver = await asyncio.start_server(_server_handler, config["pux"]["host"], config["pux"]["port"])
    addrs = ', '.join("%s:%s" % sock.getsockname() for sock in aioserver.sockets)  # pylint: disable=consider-using-f-string
    LOGGER.info("Serving on %s ", addrs)

    async with aioserver:
        try:
            await aioserver.serve_forever()
        except asyncio.exceptions.CancelledError:
            LOGGER.info("Initiating shutdown")
            LOGGER.info("Hit CTRL-C again or send a second signal to force shutdown")
            try:
                aioserver.close()
                await aioserver.wait_closed()
                LOGGER.info("Shutdown complete")
                return constants.ECODE_OK
            except asyncio.exceptions.CancelledError:
                LOGGER.warning("Forced shutdown")
                return constants.ECODE_FORCED_SHUTDOWN


def _get_nginx_certificates():
    """
    Read NGINX configuration file and returns a mapping
    of certificates with respective key and server names
    """

    nginx = crossplane.parse(config["nginx"]["config"], catch_errors=True, combine=True)

    certificates = {}

    # The first two for don't count because combine always returns a single element
    # They are here just in case
    for conf in nginx.get("config", []):
        for http in filter(lambda x: x["directive"] == "http", conf.get("parsed", [])):
            for ng_server in filter(lambda y: y["directive"] == "server", http.get("block", [])):
                fqdns = []
                certificate_path = None
                certificate_key = None
                for line in ng_server.get("block", []):
                    if line["directive"] == "server_name":
                        # Get FQDN
                        fqdns.extend(line["args"])
                    elif not certificate_path and line["directive"] == "ssl_certificate":
                        # Get certificate file
                        certificate_path = line["args"][0]
                    elif not certificate_key and line["directive"] == "ssl_certificate_key":
                        # Get certificate key
                        certificate_key = line["args"][0]
                if certificate_path and certificate_key and fqdns:
                    certificates.setdefault(certificate_path, {"names": [], "key": certificate_key})["names"].extend(fqdns)
                    LOGGER.debug("Found %s %s for %s", certificate_path, certificate_key, ",".join(fqdns))

    return certificates


async def generate():
    """
    Reads NGINX configuration file and issue certificates

    Does require crossplane library
    """
    if not crossplane:
        return constants.ECODE_CROSSPLANE_NOT_INSTALLED

    certificates = _get_nginx_certificates()

    for certificate, data in certificates.items():
        LOGGER.info("Issuing certificate for %s", ",".join(data["names"]))
        rcode, _, _ = await uacme.issue(data["names"])
        if rcode == 1:
            LOGGER.info("Certificate %s is still valid. Ignoring", certificate)
            continue
        uacme_identifier = data["names"][0]
        while uacme_identifier.startswith("*."):
            uacme_identifier = uacme_identifier[2:]
        _, failed = await pux.sendfile(certificate, os.path.join(config["uacme"]["path"], uacme_identifier, "cert.pem"))
        if failed:
            LOGGER.error("Certificate installation failed for %s", certificate)
            return constants.ECODE_CERT_INSTALL_FAILED
        _, failed = await pux.sendfile(data["key"], os.path.join(config["uacme"]["path"], "private", uacme_identifier, "key.pem"))
        if failed:
            LOGGER.error("Certificate key installation failed for %s", data["key"])
            return constants.ECODE_CERT_INSTALL_FAILED
    return constants.ECODE_OK
