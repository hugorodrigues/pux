# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
CLI commands
"""
# flake8: noqa

# This relies on each of the submodules having an __all__ variable.
from .uacme import *
from .pux import *

# pylint gets confused by pux
__all__ = (uacme.__all__ + pux.__all__)  # pylint: disable=no-member
