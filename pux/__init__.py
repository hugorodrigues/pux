# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
pux - Python + uacme + NGINX

pux generates SSL certificates using uacme based on a NGINX configuration file.
It can use both tls-alpn-01 and http-01 challenges
"""
