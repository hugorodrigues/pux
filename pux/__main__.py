# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
Application entrypoint
"""

# We import pux stuff after enabling warnings
# main() has one return per command
# pylint: disable=wrong-import-position,too-many-return-statements

import asyncio
import logging.handlers
import signal
import sys
import warnings

# enable deprecation and runtime warnings
warnings.simplefilter('default', category=DeprecationWarning)
warnings.simplefilter('default', category=RuntimeWarning)

from pux import cli  # noqa: E402
from pux.tools import constants, config  # noqa: E402

LOG_HANDLER = logging.StreamHandler()
LOG_HANDLER.setFormatter(logging.Formatter("%(asctime)s %(levelname)s %(name)s: %(message)s"))

LOGGER = logging.getLogger()
LOGGER.addHandler(LOG_HANDLER)

LOGGER.setLevel(config["pux"]["loglevel"])

# setup logging to syslog
if config["pux"]["syslog"]:
    syslog = logging.handlers.SysLogHandler("/dev/log")
    syslog.setFormatter(logging.Formatter("pux: %(levelname)s %(name)s %(message)s"))
    LOGGER.addHandler(syslog)

LOGGER.debug("pux executable to uacme hooks will be %s", constants.PUX_EXE)


def terminate():
    """
    System signal to terminate application
    """
    for task in asyncio.all_tasks():
        LOGGER.debug("Cancelling task %s", task.get_name())
        task.cancel()


async def main():
    """
    async main funcion
    """

    if len(sys.argv) == 1:
        LOGGER.error("You must provide a command")
        return constants.ECODE_INVALID_COMMAND

    for sig in (signal.SIGINT, signal.SIGTERM):
        asyncio.get_running_loop().add_signal_handler(sig, terminate)

    command = sys.argv[1]
    if command == "parse":
        return constants.ECODE_OK
    if command == "server":
        return await cli.server()
    if command == "generate":
        return await cli.generate()
    if command == "begin":
        return await cli.begin()
    if command == "done":
        return await cli.done()
    if command == "failed":
        return await cli.failed()
    if command == "new":
        return await cli.new()
    LOGGER.error("%s is not a valid command", command)
    return constants.ECODE_INVALID_COMMAND

sys.exit(asyncio.run(main()))
