# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
Functions to integrate with ualpn
"""

import asyncio
import logging

from pux.tools import config
from pux.tools.exceptions import UalpnError

LOGGER = logging.getLogger(__name__)

__all__ = ["auth", "unauth"]


async def _send(command, raise_on_error=True):
    """
    Send a command to ualpn socket

    If raise_on_error is True and ualpn returns any value other than OK, it will raise a UalpnError.
    """
    if not isinstance(command, str):
        raise TypeError("command must be a string")
    if command[-1] != "\n":
        command += "\n"

    reader, writer = await asyncio.open_unix_connection(config["ualpn"]["socket"])
    writer.write(command.encode("ascii"))
    await writer.drain()

    raw = await reader.read(1024)
    writer.close()
    response = raw.decode("ascii")
    if response != "OK\n":
        LOGGER.error("ualpn error: %s", response)
        if raise_on_error:
            raise UalpnError(response)
    return response


async def auth(ident, authentication, raise_on_error=True):
    """
    Calls auth command
    """
    return await _send(f"auth {ident} {authentication}\n", raise_on_error)


async def unauth(ident, raise_on_error=True):
    """
    Calls unauth command
    """
    return await _send(f"unauth {ident}\n", raise_on_error)
