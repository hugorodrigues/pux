# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

"""
pux server tester.
It sends a echo command to all servers
"""

import asyncio
import logging
from pux.pux import send

LOG_HANDLER = logging.StreamHandler()
LOG_HANDLER.setFormatter(logging.Formatter("%(asctime)s %(levelname)s %(name)s: %(message)s"))

LOGGER = logging.getLogger()
LOGGER.addHandler(LOG_HANDLER)
LOGGER.setLevel(logging.DEBUG)


async def echo():
    """
    async echo
    """
    results, _ = await send("echo", "test", "1", "2", "3")
    for server, result in results.items():
        print(f"{server}: {result.returncode} {result.message}")

asyncio.run(echo())
